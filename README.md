<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>


#### Тест задание

###### Необходимо написать CRUD для сущности Article, что бы все тесты должны быть успешными.

Команда для запуска теста:
```shell
./vendor/bin/phpunit
```
