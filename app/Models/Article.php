<?php

namespace App\Models;

use App\Http\Contract\Criteria\Criteria;
use Database\Factories\ArticleFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;

    protected static function newFactory()
    {
        return ArticleFactory::new();
    }

    public function scopeFilter($query, Criteria $filters)
    {
        return $filters->apply($query);
    }
}
