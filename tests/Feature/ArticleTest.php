<?php

namespace Tests\Feature;

use App\Models\Article;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;


class ArticleTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    /**
     *
     * @return void
     */
    public function test_get_articles()
    {
        Article::factory()->count(50)->create();

        $response = $this->get(route('articles.index', ['page' => 2]));

        $response->assertStatus(200)
            ->assertJson([
                'meta' => [
                    'current_page' => 2
                ]
            ])
            ->assertJsonStructure([
                'data' => [
                    [
                        'id',
                        'title',
                        'description',
                    ]
                ],
                'links' => [
                    'first',
                    'last',
                    'prev',
                    'next',
                ],
                'meta' => [
                    'current_page',
                    'from',
                    'last_page',
                    'links',
                ]
            ]);
    }

    public function test_get_article_by_filter()
    {
        $filterCount = 6;
        $filterName = 'kulan';

        Article::factory()->count(10)->create();

        Article::factory()->count($filterCount)->create([
            'title' => "{$filterName}_{rand(0, 10)}"
        ]);

        $response = $this->get(route('articles.index', ['title' => $filterName]));

        $response->assertStatus(200)
            ->assertJson([
                'meta' => [
                    'total' => $filterCount
                ]
            ])
            ->assertJsonStructure([
                'data' => [
                    [
                        'id',
                        'title',
                        'description',
                    ]
                ],
                'links' => [
                    'first',
                    'last',
                    'prev',
                    'next',
                ],
                'meta' => [
                    'current_page',
                    'from',
                    'last_page',
                    'links',
                ]
            ]);
    }

    public function test_get_article_by_id()
    {
        $this->withoutExceptionHandling();

        $article = Article::factory()->create();

        $response = $this->get(route('articles.show', ['article' => $article->id]));

        $response->assertStatus(200)
            ->assertJson([
                'data' => [
                    'title' => $article->title
                ]
            ])
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'title',
                    'description',
                ]
            ]);
    }

    public function test_create_article()
    {
        $this->withoutExceptionHandling();

        $article = Article::factory()->make();

        $response = $this->postJson(route('articles.store', [
            'title' => $article->title,
            'description' => $article->description,
        ]));

        $response->assertStatus(201)
            ->assertJson([
                'data' => [
                    'title' => $article->title,
                    'description' => $article->description
                ]
            ])
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'title',
                    'description',
                ]
            ]);
    }

    public function test_update_article()
    {
        $this->withoutExceptionHandling();

        $article = Article::factory()->create();

        $data = [
            'title' => $this->faker->name,
            'description' => $this->faker->text,
        ];

        $response = $this->putJson(route('articles.update', ['article' => $article->id]), $data);

        $response->assertStatus(200)
            ->assertJson([
                'data' => $data
            ])
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'title',
                    'description',
                ]
            ]);
    }

    public function test_delete_article()
    {
        $this->withoutExceptionHandling();

        $article = Article::factory()->create();

        $response = $this->delete(route('articles.destroy', ['article' => $article->id]));

        $response->assertStatus(204);
    }
}
